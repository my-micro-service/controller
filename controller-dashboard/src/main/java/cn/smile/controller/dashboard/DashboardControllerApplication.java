package cn.smile.controller.dashboard;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * <p>
 * 请求处理层
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
@SpringBootApplication(scanBasePackages = "cn.smile")
@MapperScan(basePackages = "cn.smile.repository.core.mapper")
public class DashboardControllerApplication {

    private static final Logger logger = LoggerFactory.getLogger(DashboardControllerApplication.class);

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(DashboardControllerApplication.class, args);
        logger.info("---------------------DashboardControllerApplication Successful Start---------------------");
    }
}

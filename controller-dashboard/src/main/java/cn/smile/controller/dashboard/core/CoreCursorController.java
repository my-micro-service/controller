package cn.smile.controller.dashboard.core;

import cn.smile.business.core.service.ICoreCursorService;
import cn.smile.commons.bean.domain.core.CoreCursor;
import cn.smile.commons.response.ResponseResult;
import cn.smile.commons.utils.RandomUtils;
import cn.smile.commons.utils.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.smile.commons.base.BaseController;

import java.util.List;

/**
 * <p>
 * MyBatis流式查询Demo 前端控制器
 * </p>
 *
 * @author 龙逸
 * @since 2020-12-31
 */
@RestController
@RequestMapping("core1/cursor")
public class CoreCursorController extends BaseController<CoreCursor, ICoreCursorService> {

    private static final Logger logger = LoggerFactory.getLogger(CoreCursorController.class);

    /**
     * MyBatis流式查询
     *
     * @param limit 页数
     * @return 查询结果
     */
    @GetMapping(value = "getCursor/{limit}")
    public ResponseResult getCursor(@PathVariable("limit") int limit) {
        logger.info("[CoreCursorController].[getCursor] ------> In, limit = {}", limit);

        List<CoreCursor> list = service.getCoreCursor(limit);

        if (CollectionUtils.isEmpty(list)) {
            return ResponseResult.failure();
        }

        return ResponseResult.success(list);
    }

    /**
     * 生成数据
     *
     * @param number 生成条数
     * @return 请求结果
     */
    @GetMapping(value = "createData/{number}")
    public ResponseResult createData(@PathVariable("number") int number) {
        ThreadPoolUtil.newTask(() -> {
            for (int i = 0; i < number; i++) {

                CoreCursor coreCursor = new CoreCursor();
                coreCursor.setText(RandomUtils.generateString(30));

                service.create(coreCursor);
            }
        });


        return ResponseResult.success();
    }
}

package cn.smile.controller.dashboard.core;

import cn.smile.business.core.service.ICorePostService;
import cn.smile.commons.bean.domain.core.CorePost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.smile.commons.base.BaseController;

/**
 * <p>
 * 文章表 前端控制器
 * </p>
 *
 * @author 龙逸
 * @since 2020-10-28
 */
@RestController
@RequestMapping("core/post")
public class CorePostController extends BaseController<CorePost, ICorePostService> {

    private static final Logger logger = LoggerFactory.getLogger(CorePostController.class);

}

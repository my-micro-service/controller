package cn.smile.controller.dashboard.core;

import cn.smile.business.core.service.ICoreUserService;
import cn.smile.commons.base.BaseController;
import cn.smile.commons.response.ResponseResult;
import cn.smile.commons.bean.domain.core.CoreUser;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
@RestController
@RequestMapping("core/user")
public class CoreUserController extends BaseController<CoreUser, ICoreUserService> {

    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * 新增
     *
     * @param coreUser {@link CoreUser}
     * @return {@link ResponseResult}
     */
    @Override
    @PostMapping("create")
    public ResponseResult create(@Valid @RequestBody CoreUser coreUser) {
        // 业务逻辑
        coreUser.setPassword(bCryptPasswordEncoder.encode(coreUser.getPassword()));
        return super.create(coreUser);
    }

    /**
     * 修改
     *
     * @param coreUser {@link CoreUser}
     * @return {@link ResponseResult}
     */
    @Override
    @PutMapping("update")
    public ResponseResult update(@Valid @RequestBody CoreUser coreUser) {

        // 业务逻辑
        coreUser.setPassword(bCryptPasswordEncoder.encode(coreUser.getPassword()));
        return super.update(coreUser);
    }
}
